import os
import torch
import logging
import setup_logger
import random
import json
from torch_geometric.data import Data, Dataset
import torch.nn.functional as F


class TreeDataset(Dataset):
    def __init__(self, root, transform=None, pre_transform=None, list_filename="filelist", extension=""):
        self.logger = logging.getLogger('Dataset.Tree')
        self.max_file_nodes = 25000
        self.max_kit_nodes = 1000000
        self.loaded_data = None
        self.list_filename = list_filename
        self.extension = extension
        self.types = None
        self._current_index = None
        self._current_data= None
        super(TreeDataset, self).__init__(root, transform, pre_transform)

        self.filenames = []
        with open(self.filelist_path) as file:
            for line in file:
                self.filenames.append(line.strip())

    @property
    def raw_path(self):
        return os.path.join(self.root)

    @property
    def filelist_path(self):
        return os.path.join(self.root, self.list_filename)

    def get_type_id(self, element):
        try:
            i = self.types.index(element.upper())
        except ValueError:
            if element.upper().endswith("MemberDeclaration".upper()): # Gather all member declaration to one id
                return self.types.index("MemberDeclaration".upper())
            elif element.upper().endswith("MethodStatement".upper()): # Gather all member statement to one id
                return self.types.index("MethodStatement".upper())
            self.logger.warning(f"Unknown Node Type : {element.upper()}")
            i = len(self.types)
        return i

    def get_type_name(self, id):
        try:
            type = self.types[id]
        except IndexError:
            self.logger.warning(f"Unknown Node Type ID : {id}")
            type = "UNKNOWN"
        return type

    def len(self):
        return len(self.filenames)

    def get(self, idx):
        if self._current_index == idx:
            return self._current_data
        nodes_id = []
        nodes_feat = []
        edges = []
        with open(os.path.join(self.root, self.filenames[idx]+self.extension)) as json_file:
            data = json.load(json_file)
            for v in data:
                if v[0] == "type":
                    nodes_id.append(int(v[1]))
                    nodes_feat.append(self.get_type_id(v[2]))
                elif v[0] == "ast_succ":
                    edges.append([
                        int(v[1]),
                        int(v[2])
                    ])
        # Edges list refer to node IDs, need to be translated to local list IDs
        for i in range(0, len(edges)):
            edges[i] = [
                nodes_id.index(edges[i][0]),
                nodes_id.index(edges[i][1])
            ]

        data = Data(
            x=F.one_hot(torch.LongTensor(nodes_feat), num_classes=self.num_node_features).type(torch.FloatTensor),
            edge_index=torch.LongTensor(edges).t(),
        )
        data.ids = torch.tensor(nodes_id)
        data.file_id = idx
        if self.pre_transform is not None:
            data = self.pre_transform(data)
        self._current_index = idx
        self._current_data = data
        return data

    def get_n_nodes(self, idx):
        if self._current_index == idx:
            return len(self._current_data.x)
        l=0
        with open(os.path.join(self.root, self.filenames[idx]+self.extension)) as json_file:
            data = json.load(json_file)
            for v in data:
                if v[0] == "type":
                    l+=1
        return l

    def shuffle(self, return_perm=False):
        random.shuffle(self.filenames)

    @property
    def num_node_features(self):
        return len(self.types)+1
