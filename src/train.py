import time
import signal
import sys
import torch
import logging
import setup_logger
import numpy as np
from torch_geometric.loader import DataLoader
from torch_geometric.nn import VGAE
from torch_geometric import utils
from model.Encoder import VariationalGCNEncoder as VariationalGCNEncoder
from Dataset.ParseTreeJavaDataset import ASTJavaDataset

logger = logging.getLogger('VGAE.main')

if len(sys.argv) != 3 and len(sys.argv) != 4:
    logger.error("Missing arguments : <name> <folder_name> [weights_file]")
    sys.exit(-1)

name = sys.argv[1]
folder = sys.argv[2]
save_file = None
if len(sys.argv) == 4:
    save_file = sys.argv[3]


last_epoch_weight = None
def signal_handler(sig, frame):
    if last_epoch_weight is not None:
        id = time.strftime("%Y%m%d-%H%M%S")
        torch.save(model.state_dict(), f'./vgae-model-files-{name}-{id}.pt')
        logger.info(f'Filename : vgae-model-files-{name}-{id}.pt')
    sys.exit(10)
signal.signal(signal.SIGINT, signal_handler)


def train(loader):
    model.train()
    loss_tot = 0
    i = 0
    for data in loader:
        data.to(device)
        optimizer.zero_grad()
        z = model.encode(data.x, data.edge_index)
        loss = model.recon_loss(z, data.edge_index)
        loss_tot += loss
        loss.backward()
        optimizer.step()
        i += 1
        if logger.level == logging.DEBUG:
            if i == 1:
                print(f"Batch {i}/{len(loader)}", end="\r")
            else:
                print(f"\rBatch {i}/{len(loader)}", end="")
            logger.debug("")
    return float(loss_tot)


def compute_neg_edges(z, data):
    # Do not include self-loops in negative samples
    pos_edge_index, _ = utils.remove_self_loops(data.edge_index)
    pos_edge_index, _ = utils.add_self_loops(pos_edge_index)
    neg_edge_index = utils.negative_sampling(pos_edge_index, z.size(0))
    return neg_edge_index


@torch.no_grad()
def test(loader):
    #logger.debug("Start test")
    model.eval()
    roc_tot = 0.0
    ap_tot = 0.0
    for data in loader:
        try:
            data.to(device)
            z = model.encode(data.x, data.edge_index)
            roc, ap = model.test(z, data.edge_index, compute_neg_edges(z, data))
            roc_tot += roc
            ap_tot += ap
            if logger.level == logging.DEBUG:
                n = model.decoder.forward_all(z, sigmoid=True)
                decoded_edges_index, _ = utils.dense_to_sparse(n >= 0.5)
                logger.debug(f"I wanted : {data.edge_index.shape} - I got : {decoded_edges_index.shape}")
                #print(data.edge_index)
                #print(decoded_edges_index)
        except (IndexError, RuntimeError, TypeError, NameError, ValueError):
            logger.error(f"Error on data : {data}")

    #logger.debug("Done test")
    return roc_tot / len(loader), ap_tot / len(loader)


start_time = time.time()
out_channels = 10
epochs = 30
batch_size = 10
dataset = ParseTreeJavaDataset(f"./data/{folder}/", extension="")

logger.info(f"Computing mask")
mask = np.ones(len(dataset), dtype=bool)
for i in range(0, len(dataset)):
    if dataset.get_n_nodes(i) > 4000: # Mask away large graphs
        mask[i] = False
logger.info(f"Skip {len(dataset)-mask.sum()}/{len(dataset)} ({100-(100*mask.sum()/len(dataset))}%)") 
logger.info(f"Done")

start_time = time.time()
train_dataset=dataset[mask]
tests_dataset=train_dataset
model = VGAE(VariationalGCNEncoder(dataset.num_node_features, out_channels))
#device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
device = torch.device('cpu')
model = model.to(device)
train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
tests_loader = DataLoader(tests_dataset, batch_size=batch_size, shuffle=True)
optimizer = torch.optim.Adam(model.parameters(), lr=5e-3)

if save_file is not None:
    logger.info(f"Loading {save_file}")
    model.load_state_dict(torch.load(f"./{save_file}"))
logger.info("--- %s seconds ---" % (time.time() - start_time))

losses = []
rocs = []
for epoch in range(1, epochs + 1):
    loss = train(train_loader)
    losses.append(loss)
    last_epoch_weight = model.state_dict()
    logger.info(f"Epoch {epoch} / {epochs} done.")
    logger.info("--- %s seconds ---" % (time.time() - start_time))

logger.info("Done.")
logger.info("--- %s seconds ---" % (time.time() - start_time))
auc, ap = test(tests_loader)
logger.info('Epoch: {:03d} - Loss: {:.4f} - AUC: {:.4f} - AP: {:.4f}'.format(epoch, loss, auc, ap))
id=time.strftime("%Y%m%d-%H%M%S")
torch.save(model.state_dict(), f'./vgae-model-files-{name}-{id}.pt')
logger.info(f'Filename : vgae-model-files-{name}-{id}.pt')

print(losses)


