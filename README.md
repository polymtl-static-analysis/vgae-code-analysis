# Unsupervised Graph Neural Networks and Centroid Distances for Source Code Similarity Detection
This repository provides a model able to learn source code similarity from AST in an unsupervised manner.  
Note that this repository does not contains any parser, but only the minimal code for training and inference.
After generating the AST, you have to create your own AST loader ( our AST loader is in src/Dataset, see [documentation](https://pytorch-geometric.readthedocs.io/en/latest/notes/create_dataset.html) )

In addition, we provided the pre-trained weights that produced the results we presented in the related article.

## Dependencies
- python
- pip

## Librairies
```bash
pip install -r src/requirements.txt
pip install torch-scatter torch-sparse torch-cluster torch-spline-conv torch-geometric -f https://data.pyg.org/whl/torch-1.11.0+cpu.html
```

## Usage
Put your data under ./data/folder_name, then once your AST reader is ready,  
you can train your model, save the weights and then infer representation for each tree.

### Training 
```bash
cd src
python train.py mymodel folder_name
```


### Inference 
See the Notebook [infer.ipynb](./src/infer.ipynb)
